## docker multi-stage build

This repository contains bash scripts to correctly build and cache a docker image with multi stages.

## What it does

Building small docker images typically requires 2 steps: 

1. from a image with dev tools such as gcc (builder), compile and install the environment
2. from an image without dev tools, copy the environment from the build step

Doing this leads to compact, neat docker images. However, the resulting image has no layers about 
the intermediary steps from the builder, and thus cannot be used as a cache.

This script performs this operation for you.

## How to use

See [.gitlab-ci.yml].
