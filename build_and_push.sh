export IMAGE_NAME=$1
export IMAGE_TAG=$2
export REF_NAME=$3
shift 3
export OTHERS=${@}
set -e

# list of all builder images
export BUILDER_TARGETS=$(sed -n "s/^FROM.* AS \(\S*\)/\1/p" Dockerfile)

build_stage () {
    docker pull "${IMAGE_NAME}/${1}:latest" || true
    export CACHE_FROM="$CACHE_FROM --cache-from ${IMAGE_NAME}/${1}:latest"
    docker build . \
        ${CACHE_FROM} \
        ${OTHERS} \
        -t "${IMAGE_NAME}/${1}:latest" \
        --target ${1}
}

export CACHE_FROM=""
for target in $BUILDER_TARGETS
do
    echo "Building stage ${target}..."
    build_stage $target
done

echo "Building image..."
docker pull "${IMAGE_NAME}:latest" || true
docker build . \
    ${CACHE_FROM} \
    --cache-from "${IMAGE_NAME}:latest" \
    ${OTHERS} \
    -t "${IMAGE_NAME}:latest"

echo "Pushing image..."
docker tag "${IMAGE_NAME}:latest" "${IMAGE_NAME}:${IMAGE_TAG}"
docker push "${IMAGE_NAME}:${IMAGE_TAG}"

if [ "${REF_NAME}" == "master" ]; then
    for target in $BUILDER_TARGETS
    do
        echo "Pushing builder ${target}..."
        docker push "${IMAGE_NAME}/${target}:latest"
    done
fi
