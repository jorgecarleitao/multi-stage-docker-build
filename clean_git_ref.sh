if [ -n "${CI_COMMIT_TAG}" ]; then
    # remove "v" from the tag
    echo "${CI_COMMIT_TAG:1}"
else
    if [ "${CI_COMMIT_REF_NAME}" == "master" ]; then
        echo "latest"
    else
        echo ${CI_COMMIT_REF_NAME}
    fi
fi
