# the image that we use to build the frontend
FROM node:13.8.0-alpine3.10 AS frontend-builder

RUN mkdir -p frontend/build/ && touch frontend/build/index.html

# the image that we use to build the backend
FROM debian:buster AS backend-builder

RUN mkdir -p target/release && touch target/release/app

# the image that runs the server
FROM debian:buster-slim

COPY --from=backend-builder target/release/app .

COPY --from=frontend-builder frontend/build/ /static

ARG arg1
ENV arg1=${arg1}
RUN echo $arg1

EXPOSE 80

CMD arg1 app
